
  'use strict';
  angular
  .module('myApp.view1',[])
  .controller('View1Ctrl', View1Ctrl);

  View1Ctrl.$inject = ['$location', 'AuthenticationService'];
function View1Ctrl($location, AuthenticationService) {
  var View1Ctrl = this;

  View1Ctrl.login = login;

  (function initController() {
  })();

  function login() {
    View1Ctrl.dataLoading = true;
     if (AuthenticationService.login(View1Ctrl.username, View1Ctrl.password))
              $location.path('/');
   
  };
}