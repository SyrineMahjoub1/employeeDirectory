'use strict';
angular.
  module('myApp').
  service('Employee', ['$resource',
    function($resource) {
     return  $resource('Employee/:Id.json', {}, {
        query: {
          method: 'GET',
          params: {lpId: 'Id'},
          isArray: true
        }
      });
    }
  ]);