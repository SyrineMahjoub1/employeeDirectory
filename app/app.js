'use strict';

// Declare app level module which depends on views, and core components
angular.module('myApp', [
  'ngRoute',
  'myApp.view1',
  'myApp.view2',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.when('/', {
    controller: 'View2Ctrl',
    templateUrl: 'view2/view2.html',
    controllerAs: 'View2Ctrl'
})

.when('/view1', {
    controller: 'View1Ctrl',
    templateUrl: 'view1/view1.html',
    controllerAs: 'View1Ctrl'
})

.otherwise({ redirectTo: '/#!/view1' });
}]);
